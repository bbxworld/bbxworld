/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, { Component } from 'react';
import { Platform, StyleSheet, Text, View } from 'react-native';

const instructions = Platform.select({
  ios: 'Press Cmd+R to reload,\n' + 'Cmd+D or shake for dev menu',
  android:
    'Double tap R on your keyboard to reload,\n' +
    'Shake or press menu button for dev menu',
});

export default class App extends Component {
  render() {
    return (
      <View style={styles.container}>
        <View style={styles.logo}>
          <Text >Logo</Text>
        </View>
        <View style={styles.currentView}>
          <Text >Current View</Text>
        </View>
        <View style={styles.viewChoices}>
          <Text >Different View Choices</Text>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  logo: {
    flex: 1,
    backgroundColor: 'red',
  },
  currentView: {
    flex: 8,
    backgroundColor: 'orange',
  },
  viewChoices: {
    flex: 1,
    backgroundColor: 'yellow',
  },
});
